# coding: utf-8

"""
@version : python3.6
@Author  : PenGoFox
@Explain : 实现模态文本获取对话框
@contact : lys2373326690@163.com
@Time    : ${DATE} ${TIME}
@File    : ${NAME}
@Software: PyCharm
"""

import tkinter as tk
from tkinter import messagebox

class PlanTextGetterToplevel(tk.Toplevel):
    def __init__(self, master, title, geometry, originalText=""):
        '''
        创建一个模态对话框，用于获取 Plan 的文本

        :param master: 父级窗口
        :param title: 窗口标题
        :param geometry: 窗口的大小和位置参数，具体为 (width, height, left, top)
        :param originalText: 传入初始文本并显示和选中，默认为空
        '''

        super().__init__(master)

        self._master = master
        self._title = title
        self._width = geometry[0]
        self._height = geometry[1]
        self._left = geometry[2]
        self._top = geometry[3]
        self._originalText = originalText

        self._initWindow()
        self._createWidget()

        self.resizable(width=False, height=False) # 设置窗口大小不可变化

        self.grab_set() # 捕获所有事件，变成模态对话框

        self.info = None # 初始化信息为 None

    def _initWindow(self):
        self.title(self._title)
        self.geometry("{}x{}+{}+{}".format(self._width, self._height, self._left, self._top))

    def _createWidget(self):
        '''
        绘制主要界面

        :return: 无返回值
        '''

        line = tk.Frame(self)
        line.pack()

        # 提示标签
        label = tk.Label(line, text="Plan Text:")
        label.pack(side=tk.LEFT, fill=tk.BOTH)

        # 输入框
        self.text = tk.StringVar()
        entry = tk.Entry(line, textvariable=self.text)
        self._entry = entry
        # 假如有初始文本，设置输入框的内容为初始文本并选中
        if self._originalText.strip() != "":
            self.text.set(self._originalText)
            entry.select_range(0, tk.END)
        entry.focus() # 输入框获取焦点
        entry.bind("<Return>", lambda x: self.ok()) # 绑定回车键为确定操作
        entry.pack(side=tk.RIGHT, fill=tk.BOTH)

        line2 = tk.Frame(self)
        line2.pack()

        okButton = tk.Button(line2, text="Ok", command=self.ok) # ok 按钮
        cancleButton = tk.Button(line2, text="Cancel", command=self.cancel) # cancel 按钮
        okButton.pack(side=tk.LEFT, fill=tk.BOTH)
        cancleButton.pack(side=tk.RIGHT, fill=tk.BOTH)

    def ok(self):
        '''
        按下 ok 按钮的回调函数

        :return: 无返回值
        '''

        text = self.text.get()
        # 假如输入框内容为空（包括空字符的情况）
        if text == "" or text.strip() == "":
            messagebox.showerror(title="Please input plan text!",
                                 message="Your input is empty, Please input plan text")
            self._entry.delete(0, tk.END) # 清空输入框内容
            self._entry.focus()
        else: # 假如输入框内容有效
            self.info = [self.text.get()] # 设置信息
            self.grab_release() # 释放捕获
            self.destroy()

    def cancel(self):
        '''
        按下 cancel 按钮的回调函数

        :return: 无返回值
        '''

        self.info = None
        self.grab_release()
        self.destroy()
