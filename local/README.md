# 本地程序实现
- [x] 先在本地快速开发实现基本的架构。

## 项目展示

![local-view.png](local-view.png)

## 项目打包成 Windows 系统可执行文件

可以把本项目打包成 Windows 系统下的可执行文件，步骤如下：

```bash
cd \local
mkdir Pyinstaller
copy *.py Pyinstaller
cd Pyinstaller
pyinstaller -F -w main.py
```

打包成功之后，可以看到 Pyinstaller 文件夹下有一个 dist 文件夹，里面的 main.exe 就是打包好的可执行文件。

> 注意事项：
>
> 要使用 pyinstaller 命令时必须要先安装 PyInstall 包，安装步骤如下：
>
> ```bash
> pip install pywin32
> pip install PyInstaller
> ```
>
> 假如 pyinstaller 命令无效，可以使用 `python pyinstaller` 代替。

## 方案

- [x] 确定数据结构和项目架构
- [x] 使用 Yaml 保存数据信息

~~实现控制台操作~~ 

- [x] 实现可视化操作

## 实现
### 确定数据结构和项目架构
#### 确定数据结构
- plan 一条计划
  - timestamp 创建计划时的时间戳（使用 time.time() 函数获取的浮点数）
  - msg 计划内容
  - done 是否已完成
#### 项目架构
- 实现计划的增删改查，设置计划已完成或未完成

### 使用 Yaml 保存数据信息

直接使用 yaml 的自动序列化和反序列化对 Todo 列表进行序列化和反序列化进行数据的保存和重建。

### 实现可视化操作

使用 tkinter 框架创建窗口并实现操作