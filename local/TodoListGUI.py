# coding: utf-8

"""
@version : python3.6
@Author  : PenGoFox
@Explain : 图形界面实现
@contact : lys2373326690@163.com
@Time    : ${DATE} ${TIME}
@File    : ${NAME}
@Software: PyCharm
"""

import tkinter as tk
from VerticalScrolledFrame import VerticalScrolledFrame
from PlanTextGetterToplevel import PlanTextGetterToplevel

import platform
import system_hotkey

class TodoListGUI(tk.Frame):
    def __init__(self, master, geometry, title, addPlanFeedback=None, planOperateFeedback=None):
        '''
        TodoList 的GUI界面

        :param master: 父级，通过 tk.Tk() 获得
        :param geometry: 窗口的大小和位置参数，具体为 (width, height, left, top)
        :param title: 窗口的标题
        :param addPlanFeedback: 按下添加计划按钮的回调函数
        :param planOperateFeedback: 对计划进行操作（完成，编辑和删除）的回调函数
        '''

        super().__init__(master)
        self._master = master
        self._title = title
        self._width = geometry[0]
        self._height = geometry[1]
        self._left = geometry[2]
        self._top = geometry[3]

        self._initWindow() # 初始化窗口
        self._addOptionButton() # 添加一些功能按键
        self._createWidgets() # 绘制主要的界面

        self.pack(fill=tk.BOTH)

        self.addPlanFeedback = addPlanFeedback
        self.planOperateFeedback = planOperateFeedback

        self._master.resizable(width=True, height=False) # 只允许调整窗口的宽度

        self._isShow = True # 当前是否已经显示窗口
        self._bindHotKeyToExchangeShowSelf() # 注册显示与隐藏窗口的热键

        self._master.wm_attributes('-topmost', 1) # 置顶窗口

    def addPlanItem(self, timestamp:float, text:str, checked=False):
        '''
        添加一条计划到窗口中

        :param timestamp: 计划的时间戳
        :param text: 计划的具体内容
        :param checked: 计划的实施状态
        :return: 无返回值
        '''

        checkButtonVariable = tk.BooleanVar() # 复选框的值变量
        checkButtonVariable.set(checked)

        # 一行的容器
        lineContainer = tk.Frame(self._container)

        # 行左边的容器
        leftContainer = tk.Frame(lineContainer)
        # 复选框
        checkButton = tk.Checkbutton(leftContainer, text=text, justify=tk.LEFT,
                                     variable=checkButtonVariable,
                                     command=lambda : self._planOperate("check", timestamp, checkButtonVariable.get()))
        checkButton.pack()

        # 行右边的容器
        rightContainer = tk.Frame(lineContainer)
        # edit 按钮
        editButton = tk.Button(rightContainer, text="Edit",
                               command=lambda : self._planOperate("edit", timestamp, text))
        # delete 按钮
        deleteButton = tk.Button(rightContainer, text="Del", command=lambda : self._planOperate("delete", timestamp))
        deleteButton.pack(side="right")
        editButton.pack(side="right")

        leftContainer.pack(side="left", fill=tk.BOTH)
        rightContainer.pack(side="right", fill=tk.BOTH)

        lineContainer.pack(side="top", fill=tk.BOTH)

    def clearAllPlanItems(self):
        '''
        清除窗口内所有计划的组件，用于后续重新添加计划组件

        :return: 无返回值
        '''

        self._verticalScrolledFrame.destroy()
        self._createWidgets()

    def _planOperate(self, *args):
        '''
        根据某一计划的某一个操作对回调函数传递正确的参数

        其中操作有 "check"、"edit" 和 "delete"
        当操作为 "check" 时，值为当前的 Plan 是否被选中
        当操作为 "edit" 时，值为当前 Plan 的具体内容
        当操作为 "delete" 时，无值传递

        为回调函数传送参数时，参数格式为 :
            self, (操作, 时间戳, 值)
            当操作为 "edit" 时，值为新的内容

        :param args: 参数，格式为 (操作, 时间戳, 值)
        :return: 无返回值
        '''

        if not self.planOperateFeedback is None:
            operate = args[0]
            if operate == "edit": # 当操作为 "edit" 时获取新的内容
                editPlanToplevel = PlanTextGetterToplevel(self._master, "Edit Plan", (200, 70, self._left, self._top),
                                                          originalText=args[2])
                self.wait_window(editPlanToplevel)
                info = editPlanToplevel.info
                text = info[0] if not info is None else args[2]
                args = (args[0], args[1], text)
            self.planOperateFeedback(self, args)

    def _createWidgets(self):
        '''
        绘制主要界面

        :return: 无返回值
        '''

        verticalScrolledFrame = VerticalScrolledFrame(self)
        self._verticalScrolledFrame = verticalScrolledFrame

        self._container = verticalScrolledFrame.interior
        verticalScrolledFrame.pack(fill=tk.BOTH)

    def _addOptionButton(self):
        '''
        添加功能按钮，这里暂时只有 Add 按钮

        :return: 无返回值
        '''

        line = tk.Frame(self)
        line.pack(fill=tk.X)

        addButton = tk.Button(line, text="Add", command=lambda : self.getNewPlanText())
        addButton.pack(side=tk.RIGHT)

    def _initWindow(self):
        self._master.title(self._title)
        self._master.geometry("{}x{}+{}+{}".format(self._width, self._height, self._left, self._top))

    def getNewPlanText(self):
        '''
        点击了添加计划按钮后的动作

        :return: 无返回值
        '''

        addPlanToplevel = PlanTextGetterToplevel(self._master, "Add Plan", (200, 70, self._left, self._top))
        self.wait_window(addPlanToplevel)  # 要用这个来阻塞程序
        info = addPlanToplevel.info # 获取到数据
        if not info is None: # 当数据有效时提取数据并调用回调函数
            text = info[0]
            if not self.addPlanFeedback is None: # 当回调函数不为空时调用回调函数并传参
                self.addPlanFeedback(self, text)

    def _bindHotKeyToExchangeShowSelf(self, hotkey=('control', 't')):
        '''
        根据操作系统类型绑定全局热键用以显示或隐藏本程序

        :param hotkey: 热键，一个元组，例如：('control', 'shift', 't')
        :return: 无返回值
        '''

        # 获取操作系统类型
        systemInfo = platform.platform()
        self.systemType = systemType = systemInfo.split("-")[0]

        # 记录设置的热键
        self._hotkeyValueToExchangeShowSelf = hotkey

        # Windows 系统使用 system_hotkey 来注册热键
        if systemType == "Windows":
            self._hotkeyToExchangeShowSelf = system_hotkey.SystemHotkey() # 这个名字是通用的，不同系统使用不同方法而已
            self._hotkeyToExchangeShowSelf.register(hotkey, callback=lambda x : self._exchengeShowSelf())

    def _unbindHotKeyToExchangeShowSelf(self):
        '''
        根据操作系统类型解绑用于显示或隐藏本程序的全局热键

        :return: 无返回值
        '''

        if self.systemType == "Windows":
            self._hotkeyToExchangeShowSelf.unregister(self._hotkeyValueToExchangeShowSelf)

    def _exchengeShowSelf(self):
        '''
        根据 self._isShow 来交替显示或隐藏窗口

        :return: 无返回值
        '''

        if self._isShow:
            self._master.withdraw() # 隐藏窗口
        else:
            self._master.deiconify() # 显示窗口

        self._isShow = not self._isShow

    def destroy(self):
        '''
        销毁之前做一些工作

        :return: 无返回值
        '''

        # 退出之前注销热键
        self._unbindHotKeyToExchangeShowSelf()

        # 正式销毁
        super().destroy()