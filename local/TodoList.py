# coding: utf-8

"""
@version : python3.6
@Author  : PenGoFox
@Explain : 实现 Plan 的 todolist 操作
@contact : lys2373326690@163.com
@Time    : ${DATE} ${TIME}
@File    : ${NAME}
@Software: PyCharm
"""

from Plan import Plan
import time

class TodoList:
    def __init__(self):
        self.__todoList = []

    def _getPlanByTimestamp(self, timestamp:float):
        '''
        通过时间戳取 Plan

        :param timestamp: 时间戳
        :return: 成功返回对应 Plan 对象，失败返回 None
        '''

        for plan in self.__todoList:
            if plan.timestamp == timestamp:
                return plan
        return None

    def addPlan(self, msg:str):
        '''
        添加一条 Plan
        :param msg: Plan 的具体内容
        :return: 返回创建 Plan 时的时间戳
        '''

        timestamp = time.time()
        plan = Plan(timestamp, msg)
        self.__todoList.append(plan)
        return timestamp

    def _setPlanDoneValue(self, timestamp:float, done=True):
        '''
        设置 Plan 的完成状态

        :param timestamp: Plan 的时间戳
        :param done: 是否已经完成
        :return: 无返回值
        '''

        plan = self._getPlanByTimestamp(timestamp)
        if not plan is None:
            plan.done = done

    def donePlan(self, timestamp:float):
        '''
        设置 Plan 的状态为完成

        :param timestamp: Plan 的时间戳
        :return: 无返回值
        '''

        self._setPlanDoneValue(timestamp, True)

    def unDonePlan(self, timestamp:float):
        '''
        设置 Plan 的状态为未完成

        :param timestamp: Plan 的时间戳
        :return: 无返回值
        '''
        self._setPlanDoneValue(timestamp, False)

    def editPlan(self, timestamp:float, msg:str):
        '''
        编辑 Plan 的具体内容

        :param timestamp: Plan 的时间戳
        :param msg: Plan 的新具体内容
        :return: 无返回值
        '''
        plan = self._getPlanByTimestamp(timestamp)
        if not plan is None:
            plan.msg = msg

    def deletePlan(self, timestamp:float):
        '''
        删除 Plan

        :param timestamp: Plan 的时间戳
        :return: 无返回值
        '''
        plan = self._getPlanByTimestamp(timestamp)
        if plan in self.__todoList:
            self.__todoList.remove(plan)

    def sort(self):
        '''
        对所有 Plan 根据 timestamp 排序，未完成的在前面，完成的在后面，完成或未完成的都根据 timestamp 排序

        :return: 无返回值
        '''

        undoneList = []
        doneList = []
        for plan in self.__todoList:
            if plan.done:
                doneList.append(plan)
            else:
                undoneList.append(plan)

        undoneList.sort(key=lambda x: x.timestamp, reverse=True)
        doneList.sort(key=lambda x: x.timestamp, reverse=True)

        self.__todoList = undoneList + doneList

    def __getitem__(self, item):
        return self.__todoList[item]

    def __iter__(self):
        return self.__todoList.__iter__()

    def __next__(self):
        return self.__todoList.__next__()