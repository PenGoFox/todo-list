# coding: utf-8

"""
@version : python3.6
@Author  : PenGoFox
@Explain : 实现 Plan 数据结构
@contact : lys2373326690@163.com
@Time    : ${DATE} ${TIME}
@File    : ${NAME}
@Software: PyCharm
"""

class Plan:
    def __init__(self, timestamp:float, msg:str):
        '''
        Plan 数据类型

        :param timestamp: 创建时的时间戳，使用 time.time() 获得
        :param msg: Plan 的具体内容
        '''

        self.timestamp = timestamp
        self.msg = msg
        self.done = False