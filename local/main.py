# coding: utf-8

"""
@version : python3.6
@Author  : PenGoFox
@Explain : 项目入口
@contact : lys2373326690@163.com
@Time    : ${DATE} ${TIME}
@File    : ${NAME}
@Software: PyCharm
"""

from TodoList import TodoList
from TodoListGUI import TodoListGUI
import tkinter
import yaml
import os

if __name__ == "__main__":
    dataFileName = "data.yaml" # 数据文件的文件名

    # 数据文件存在则直接读取并反序列化为 TodoList 对象
    if os.path.exists(dataFileName):
        todoList = yaml.load(open(dataFileName, "r"), Loader=yaml.FullLoader)
        todoList.sort() # 对计划根据时间戳进行排序
    # 数据文件不存在则新建一个 TodoList 对象
    else:
        todoList = TodoList()

    def addPlanFeedback(app, text):
        '''
        添加新计划的回调函数

        :param app: TodoListGUI 对象
        :param text: 新计划的具体内容
        :return: 无返回值
        '''

        todoList.addPlan(text)
        todoList.sort()

        app.clearAllPlanItems()

        for plan in todoList:
            app.addPlanItem(plan.timestamp, plan.msg, plan.done)

    def planOperateFeedback(app, args):
        '''
        某计划被操作的回调函数

        :param app: TodoListGUI 对象
        :param args: 操作内容等参数
        :return: 无返回值
        '''

        operate = args[0] # 操作名称
        timestamp = args[1] # 计划的时间戳

        # 设置计划完成或未完成
        if operate == "check":
            value = args[2]
            if value:
                todoList.donePlan(timestamp)
            else:
                todoList.unDonePlan(timestamp)
            todoList.sort()

        # 修改计划内容
        elif operate == "edit":
            text = args[2] # 此时的 text 为更新后的内容，直接写入即可
            todoList.editPlan(timestamp, text)

        # 删除计划
        elif operate == "delete":
            todoList.deletePlan(timestamp)
            todoList.sort()
        else:
            raise TypeError

        # 清除窗口的内容
        app.clearAllPlanItems()

        # 重新写入窗口内容
        for plan in todoList:
            app.addPlanItem(plan.timestamp, plan.msg, plan.done)

    # 创建一个 tkinter 窗口
    root = tkinter.Tk()
    # 计算屏幕中心位置
    screenWidth = root.winfo_screenwidth()
    screenHeight = root.winfo_screenheight()
    windowWidth = 400 # 主窗口宽度
    windowHeight = 300 # 主窗口高度
    windowTitle = 'Todo List - PenGoFox' # 窗口标题
    top = (screenHeight - windowHeight) // 2
    left = (screenWidth - windowWidth) // 2
    geometry = (windowWidth, windowHeight, left, top)
    # 创建一个 TodoListGUI 窗口
    app = TodoListGUI(master=root, geometry=geometry, title=windowTitle,
                      addPlanFeedback=addPlanFeedback, planOperateFeedback=planOperateFeedback)
    # 添加计划
    for plan in todoList:
        app.addPlanItem(plan.timestamp, plan.msg, plan.done)

    app.mainloop()

    # 保存数据
    yaml.dump(todoList, open(dataFileName, "w"))
